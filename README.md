# Disgaea RPG - assets

This git contains the extracted assets of the game [Disgaea RPG](https://play.google.com/store/apps/details?id=com.disgaearpgtest.forwardworks) by Forwardworks.

The assets were copied from ``/Android/data/com.disgaearpgtest.forwardworks/files/assetbundle`` and extracted via [UnityPy](https://github.com/K0lb3/UnityPy).
The extraction script can be found at [Disgaea RPG/Scripts](https://gitlab.com/disgea-rpg/scripts).
The game version was 2.0.0.
